﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

   public void PulsaPlay()
    {
        Debug.Log("He pulsado play");

        SceneManager.LoadScene("Game");

    }

    public void PulsaExit()
    {
        Debug.Log("He pulsado creditos");
        Application.Quit();
    }

    public void PulsaCredits()
    {
        Debug.Log("He pulsado creditos");

        SceneManager.LoadScene("Credits");
    }

    public void PulsaMenu()
    {
        Debug.Log("Volviendo al menu");

        SceneManager.LoadScene("MainMenu");
    }
}
