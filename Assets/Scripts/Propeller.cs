﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : MonoBehaviour {

    public TrailRenderer blue01;
    public TrailRenderer blue02;
    public TrailRenderer Red01;
    public TrailRenderer Red02;


    // Use this for initialization
    void Awake () {
        Stop ();
    }
    
    public void BlueFire(){
        blue01.emitting = true;
        blue02.emitting = true;
    }

    public void RedFire() {
        Red01.emitting = true;
        Red02.emitting = true;
    }

    public void Stop(){
        blue01.emitting = false;
        blue02.emitting = false;
        Red01.emitting = false;
        Red02.emitting = false;
    }

    public void TrailDestroyed() {
        blue01.enabled = false;
        blue02.enabled = false;
        Red01.enabled = false;
        Red02.enabled = false;
    }

    public void TrailRestore()
    {
        blue01.enabled = true;
        blue02.enabled = true;
        Red01.enabled = true;
        Red02.enabled = true;
    }
}

