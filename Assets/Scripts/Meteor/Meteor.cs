﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 Speed;
    public GameObject[] meteoritos;
    int Seleccionado;
    public AudioSource audioSource;
    public ParticleSystem ps;
    public GameObject MeteorToInstantiate;


    void Awake()
    {
        for (int i = 0; i < meteoritos.Length; i++)
        {
            meteoritos[i].SetActive(false);
        }

        Seleccionado = Random.Range(0,meteoritos.Length);
        meteoritos[Seleccionado].SetActive(true);

        Speed.x = Random.Range(-5, 0);
        Speed.y = Random.Range(-4, 5);
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Speed * Time.deltaTime);
        meteoritos[Seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }
    IEnumerator DestroyMeteor(){
        //Desactivo el grafico
        meteoritos[Seleccionado].gameObject.SetActive(false);

        Destroy(GetComponent<BoxCollider2D>());

        //Particula Lanzada
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //creacion de Meteoritos
        InstanceMeteors();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }

    public virtual void InstanceMeteors()
    {
        Instantiate(MeteorToInstantiate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstantiate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstantiate, this.transform.position, Quaternion.identity, null);
        Instantiate(MeteorToInstantiate, this.transform.position, Quaternion.identity, null);
    }
}
