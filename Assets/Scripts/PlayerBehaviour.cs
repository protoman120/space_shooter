﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public float shootTime=0;
    public Weapon weapon;
    public Propeller prop;
    public GameObject ship;
    public ParticleSystem Explosion;
    public AudioSource ExplosionSound;
    private bool IamDead = false;
    public float Respawntimer;
    public int Invencibility;
    public BoxCollider2D ShipCollider;
    public int Playerlives;
    public ScoreManager Score;

    private void Update()
    {
        if (IamDead == false)
        {

            shootTime += Time.deltaTime;

            transform.Translate(axis * speed * Time.deltaTime);

            if (transform.position.x > limits.x)
            {
                transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
            }

            if (transform.position.y > limits.y)
            {
                transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
            }

            if (transform.position.x < -limits.x)
            {
                transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
            }

            if (transform.position.y < -limits.y)
            {
                transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
            }

            if (axis.x > 0)
            {
                prop.BlueFire();
            }
            else if (axis.x < 0)
            {
                prop.RedFire();
            }
            else
            {
                prop.Stop();
            }
        }

    }

        public void SetAxis(Vector2 currentaxis)
        {
            axis = currentaxis;
        }

        public void Shoot()
        {
            //Debug.Log("Dispara");
            if (shootTime > weapon.GetCadencia())
            {
                shootTime = 0;
                weapon.Shoot();
            }
        }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Meteorite")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "EnemyBullet")
        {
            StartCoroutine(DestroyShip());
        }
    }

    private void lifelost()
    {
        Playerlives--;
    }

    IEnumerator DestroyShip()
    {

        IamDead = true;

        if (IamDead == true)
        {
            ship.gameObject.SetActive(false);

            prop.TrailDestroyed();

            prop.Stop();

            ShipCollider.enabled = false;

            Explosion.Play();

            ExplosionSound.Play();

            yield return new WaitForSeconds(Respawntimer);

            lifelost();

            Score.LoseLife();

            if (Playerlives > 0)
            {
                StartCoroutine(RespawnProcess());
            }
            else if (Playerlives <= 0)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }

    }

    IEnumerator RespawnProcess()
    {

        IamDead = false;

        Explosion.Clear();
        Explosion.Stop();

        ship.gameObject.SetActive(true);

        prop.TrailRestore();

        for(int i= 0; i<Invencibility*2; i++)
        {
            ship.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            ship.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }

        ShipCollider.enabled = true;

    }
}
