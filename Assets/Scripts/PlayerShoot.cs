﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public float velocity;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(velocity * Time.deltaTime,0, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Meteorite")
        {
            Destroy(gameObject);
        }
    }

}
